import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  messages: string[] = [];

  add(message: string) {
    this.messages.push(message);
  }

  showSuccessMessage(message: string): void {
    if (message) {
      alert(message);
    } else {
      alert('Successfully performed operation.');
    }
  }

  clear() {
    this.messages = [];
  }
}
