import { Component, OnInit } from '@angular/core';

import { Client } from '../client';
import { ClientService } from '../client.service';
import { MessageService } from '../message.service';
import {ModalContentComponent} from '../modal-content/modal-content.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
  client: Client;
  clients: Client[];

  constructor(
    private clientService: ClientService,
    private messageService: MessageService,
    private modalService: NgbModal
    ) { }

  ngOnInit() {
    this.getClients();
  }

  getClients(): void {
    this.clientService.getClients()
      .subscribe(clients => this.clients = clients);
  }

  crudClientMenu(client: Client): void {
    if (!client) {
      client = {id: null, name: null, description: null};
    }
    const modalRef = this.modalService.open(ModalContentComponent);
    modalRef.componentInstance.entity = client;
    modalRef.componentInstance.passEntry.subscribe((receivedClient) => {
      this.client = receivedClient;
      this.addClient();
    });
  }

  addClient(): void {
    this.clientService.addClient(this.client)
      .subscribe(() => {
        this.messageService.showSuccessMessage('Successfully added new client!');
        this.getClients();
      });
  }

//   deleteClient(client: Client): void {
//     this.clientService.deleteClient(client)
//       .subscribe();
//     this.clients = this.clients.filter(c => c !== client);
//   }
}
