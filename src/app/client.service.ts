import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Client } from './client';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private clientApi = 'http://localhost:8080/dashboard/client';

  constructor(
    private messageService: MessageService,
    private http: HttpClient
  ) { }

  /** Log a ClientService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`ClientService: ${message}`);
  }

  getClient(id: number): Observable<Client> {
    this.messageService.add('Fetching user with ID: ' + id);
    return this.http.get<Client>(this.clientApi + '/' + id);
  }

  getClients(): Observable<Client[]> {
//     return this.http.get<Client[]>(this.clientApi)
//       .pipe(catchError(this.handleError<Client[]>('getClients', [])));
    return of([{id: 1, name: 'Standish M.', description: 'Client number 1'},
               {id: 2, name: 'Ark Investments M.', description: 'Client number 2'} ]);
  }

  /**
   * Add new client method
   * @param client - a Client that will be added to application
   */
  addClient(client: Client): Observable<Client> {
    return this.http.post(this.clientApi, client, httpOptions).pipe(
      tap(_ => this.log(`added client with name=${client.name}`)),
      catchError(this.handleError<any>('addClient'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
        console.error(error); // log to console
        this.log(`${operation} failed: ${error.message}`);
        // Keep app running by returning an empty result.
        return of(result as T);
    };
  }
}
