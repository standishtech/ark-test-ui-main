# Dashboard UI

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

##Prerequisites
1. npm version 6.x (6.0.1)
2. node version 10.x (10.0.0)

For Angular CLI installation read [Getting started guide](https://angular.io/guide/quickstart).


##Setup
Configure backend API links for service (client, etc...), so that it points to real application server.
For example:
```
private clientApi = 'http://localhost:8080/dashboard/client
```
This has to be really configured dynamically.

####Backend will need to be provide as a service (spring boot is a good option)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Task details
###UI
Have a simple dashboard with links that would allow:
- display list of clients
- When client is clicked load list of Investors
- When Investor is clicked load list of funds
- Ability to create and edit a new client. Client has name and description fields. -
Please feel free to make assumptions on any other key factors. Make sure to document your assumptions in the README file in the project.
